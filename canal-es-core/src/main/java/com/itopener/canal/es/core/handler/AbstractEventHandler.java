package com.itopener.canal.es.core.handler;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.otter.canal.protocol.CanalEntry.Column;
import com.alibaba.otter.canal.protocol.CanalEntry.Entry;
import com.alibaba.otter.canal.protocol.CanalEntry.RowChange;
import com.alibaba.otter.canal.protocol.CanalEntry.RowData;

public abstract class AbstractEventHandler implements IEventHandler {

	private final Logger logger = LoggerFactory.getLogger(AbstractEventHandler.class);

	@Override
	public void handle(Entry entry, RowChange change) {
		// 查询此数据库+表数据变更时所需要同步的数据
		for(RowData rowData : change.getRowDatasList()) {
			logger.debug("handle change row data:{}", rowData.toString());
			handle(entry, rowData);
		}
	}
	
	protected String getAfterColumnValue(RowData rowData, String columnName) {
		List<Column> columns = rowData.getAfterColumnsList();
		for(Column column : columns) {
			if(column.getName().equals(columnName)) {
				return column.getValue();
			}
		}
		return null;
	}
	
	protected String getBeforeColumnValue(RowData rowData, String columnName) {
		List<Column> columns = rowData.getBeforeColumnsList();
		for(Column column : columns) {
			if(column.getName().equals(columnName)) {
				return column.getValue();
			}
		}
		return null;
	}

	public abstract void handle(Entry entry, RowData rowData);
}
