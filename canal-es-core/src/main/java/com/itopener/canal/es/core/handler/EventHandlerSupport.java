package com.itopener.canal.es.core.handler;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.otter.canal.protocol.CanalEntry.Entry;
import com.alibaba.otter.canal.protocol.CanalEntry.RowChange;

@Component
public class EventHandlerSupport {
	
	private final Logger logger = LoggerFactory.getLogger(EventHandlerSupport.class);
	
	@Autowired
	private List<IEventHandler> eventHandlers;
	
	public void handle(Entry entry, RowChange change) {
		logger.debug("handle RowChange:{}", change);
		String database = entry.getHeader().getSchemaName();
		String table = entry.getHeader().getTableName();
		eventHandlers.forEach(eventHandler -> {
			if(eventHandler.getDatabase().equals(database)
					&& eventHandler.getTableName().equals(table)) {
				eventHandler.handle(entry, change);
			}
		});
	}
	
}
