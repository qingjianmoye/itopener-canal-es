package com.itopener.canal.es.core.handler;

import com.alibaba.otter.canal.protocol.CanalEntry.Entry;
import com.alibaba.otter.canal.protocol.CanalEntry.RowChange;

public interface IEventHandler {

	String getDatabase();
	
	String getTableName();
	
	void handle(Entry entry, RowChange change);
	
}
